/**
Projet L2 Structures Algébriques 2022
Auteur : Mohamed Akaddar
*/
#include<stdio.h>
#include<stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

#include"src/liste_figure.c"
#include"src/figures/point.c"
#include"src/couleur.c"
#include"src/figures/segment.c"
#include"src/figures/cercle.c"
#include"src/figures/rectangle.c"
#include"src/figures/triangle.c"
#include"src/figures/polygone.c"
#include"src/gestion_evenement.c"


int inialisation_SDL(SDL_Window ** fenetre, int largeur, int hauteur, SDL_Renderer ** renderer);
void fermeture_SDL(SDL_Window * fenetre, SDL_Renderer * renderer);
/*fonction qui gere les evenements*/
void gestion_evenements(int * close, SDL_Renderer* rendu, liste_figure_t* l, SDL_Surface* surface, SDL_Texture* texture,SDL_Rect rect, int* indice);
void rendu_fenetre(SDL_Renderer * renderer, liste_figure_t * l);
/*fonctionn qui affiche la liste des Raccourcis existants*/
void afficher_raccourcis();

int main(int argc, char *argv[])
{
  afficher_raccourcis();

  /*ce indice va m'aider pour savoir gerer le choix d'event*/
  int indice = 0 ;
  SDL_Window * fenetre;
  SDL_Renderer * rendu;

  SDL_Surface* surface = NULL;
  SDL_Texture* texture = NULL;

  SDL_Rect rect = {0,0,1000,600};


  int close = inialisation_SDL(&fenetre, 1000, 600, &rendu);
  if( close < 0 )
    return EXIT_FAILURE;

  liste_figure_t * l = creer_liste();


   while(!close)
   {
    gestion_evenements(&close, rendu, l, surface, texture,rect, &indice);
    rendu_fenetre(rendu, l);
   }

  fermeture_SDL(fenetre, rendu);
  return EXIT_SUCCESS;
}



void rendu_fenetre(SDL_Renderer * rendu, liste_figure_t * l)
{
  SDL_SetRenderDrawColor(rendu, 0, 0, 0, 255);
  SDL_RenderClear(rendu);

  for(maillon_figure_t * tmp = l->premier; tmp != NULL; tmp = tmp->suivant)
  {
    tmp->figure->afficher(rendu, tmp->figure);
  }
  SDL_RenderPresent(rendu);
}



/*initialisation de la SDL*/
int inialisation_SDL(SDL_Window ** fenetre, int largeur, int hauteur, SDL_Renderer ** rendu)
{
  // Quitte le programme en cas d'erreur
  if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
  {
    printf("error initializing SDL: %s\n", SDL_GetError());
    return -1;
  }
  *fenetre = SDL_CreateWindow("PROJET L2 Structures Algébriques",
			      SDL_WINDOWPOS_CENTERED,
			      SDL_WINDOWPOS_CENTERED,
			      largeur, hauteur, 0);
  *rendu = SDL_CreateRenderer(*fenetre, -1, 0);
  return 0;
}


/*fermeture de la SDL*/
void fermeture_SDL(SDL_Window * fenetre, SDL_Renderer * rendu)
{
  SDL_DestroyRenderer(rendu);
  SDL_DestroyWindow(fenetre);
  SDL_Quit();
}

/*Affichage des raccourcis*/
void afficher_raccourcis()
{
  printf("= = = = Raccourcis clavier existant = = = =\n");
  printf("q : Quitter le logiciel\n");
  printf("p : Dessiner un point\n");
  printf("l : Dessiner un segment\n");
  printf("r : Dessiner un rectangle\n");
  printf("t : Dessiner un triangle\n");
  printf("c : Dessiner un cercle\n");
  printf("x : Dessiner un polygone\n");
  printf("a : Préciser une couleur\n");
  printf("s : sauvegarder le dessin\n");
  printf("d : charger une image\n");
  printf("= = = = = = =  Fin de la liste = = = = = = =\n");
}
