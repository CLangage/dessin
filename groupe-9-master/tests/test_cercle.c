#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include"../src/figures/cercle.h"


#define ROUGE  "\x1B[31m"
#define VERT "\x1B[32m"
#define DEFAUT  "\x1B[0m"

int main(){
  printf("%s",ROUGE);

  cercle_t * c = creer_cercle(creer_point(10, 10, creer_couleur(125, 0, 210)),5, creer_couleur(125, 0, 210));

  assert(c->couleur.rouge==125);
  assert(c->couleur.vert==0);
  assert(c->couleur.bleu==210);
  assert(c->r>0);
  assert( c->detruire !=NULL );
  assert( c->afficher !=NULL );

  c->detruire(c);

  printf("%stest_cercle a passé les tests avec succès\n", VERT);
  printf("%s",DEFAUT);
  return EXIT_SUCCESS;
}
