#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include"../src/figures/segment.h"


#define ROUGE  "\x1B[31m"
#define VERT "\x1B[32m"
#define DEFAUT  "\x1B[0m"

int main(){
  printf("%s",ROUGE);

  segment_t * s = creer_segment(creer_point(10, 10, creer_couleur(125, 0, 210)),creer_point(15,15, creer_couleur(125, 50, 210)), creer_couleur(125, 50, 210));

  assert( s->detruire !=NULL );
  assert( s->afficher !=NULL );

  s->detruire(s);

  printf("%stest_segment a passé les tests avec succès\n", VERT);
  printf("%s",DEFAUT);
  return EXIT_SUCCESS;
}