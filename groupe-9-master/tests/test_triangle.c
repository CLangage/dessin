#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include"../src/figures/triangle.h"


#define ROUGE  "\x1B[31m"
#define VERT "\x1B[32m"
#define DEFAUT  "\x1B[0m"

int main(){
  printf("%s",ROUGE);

  triangle_t * t = creer_triangle(creer_point(10, 10, creer_couleur(125, 0, 210)),creer_point(15,15, creer_couleur(125, 0, 210)),creer_point(20,20, creer_couleur(125, 0, 210)), creer_couleur(125, 0, 210));

  assert(t->couleur.rouge==125 && t->couleur.vert==0 && t->couleur.bleu==210);
  assert( t->detruire !=NULL );
  assert( t->afficher !=NULL );

  t->detruire(t);

  printf("%stest_triangle a passé les tests avec succès\n", VERT);
  printf("%s",DEFAUT);
  return EXIT_SUCCESS;
}