#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include"../src/figures/polygone.h"


#define ROUGE  "\x1B[31m"
#define VERT "\x1B[32m"
#define DEFAUT  "\x1B[0m"

int main(){
  printf("%s",ROUGE);

  polygone_t * p = creer_polygone(creer_point(10, 10, creer_couleur(125, 0, 210)),5, creer_couleur(125, 0, 210));

  assert( p->detruire !=NULL );
  assert( p->afficher !=NULL );

  p->detruire(p);

  printf("%stest_polygone a passé les tests avec succès\n", VERT);
  printf("%s",DEFAUT);
  return EXIT_SUCCESS;
}
