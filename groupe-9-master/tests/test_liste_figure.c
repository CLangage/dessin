#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include"..VERT/src/couleur.c"
#include"../src/figures/point.c"
#include"../src/figures/segment.c"
#include"../src/liste_figure.h"



#define ROUGE  "\x1B[31m"
#define VERT "\x1B[32m"
#define DEFAUT  "\x1B[0m"

int main(){
  printf("%s",ROUGE);

  segment_t * s = creer_segment(creer_point(10, 10, creer_couleur(125, 0, 210)),creer_point(15,15, creer_couleur(125, 50, 210)), creer_couleur(125, 50, 210));
  maillon_figure_t *t= creer_maillon(s);
  liste_figure_t *lt=creer_liste();
  int vide =liste_est_vide(lt);
  int taille =liste_taille(lt);
  liste_inserer_debut(lt,s);
  liste_inserer_fin(lt,s);
  maillon_figure_t *f=liste_extraire_debut(lt);

  assert(liste_taille(lt)!=0);

  detruire_liste(lt);

  printf("%stest_liste_figures a passé les tests avec succès\n", VERT);
  printf("%s",DEFAUT);
  return EXIT_SUCCESS;
}