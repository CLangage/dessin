#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include"../src/figures/rectangle.h"

#define ROUGE  "\x1B[31m"
#define VERT "\x1B[32m"
#define DEFAUT  "\x1B[0m"

int main(){
  printf("%s",ROUGE);
  
  point_t * p=creer_point(10,3,creer_couleur(125, 0, 210));
  couleur_t c=creer_couleur(125, 0, 210);
  rectangle_t * r = creer_rectangle(p,2,3,c);

  assert( r->detruire !=NULL );
  assert( r->afficher !=NULL );

  r->detruire(r);

  printf("%stest_rectangle a passé les tests avec succès\n", VERT);
  printf("%s",DEFAUT);
  return EXIT_SUCCESS;
}