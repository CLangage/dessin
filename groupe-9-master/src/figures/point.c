#include"point.h"


void detruire_point(void * point){
  point_t * p = point;
  free(p);
}


void afficher_point(SDL_Renderer * rendu, void * point){
  point_t * p = point;
  SDL_SetRenderDrawColor(rendu, p->couleur.rouge,
			 p->couleur.vert, p->couleur.bleu, 255);
  SDL_RenderDrawPoint(rendu, p->x, p->y);
}

point_t * creer_point(unsigned int x, unsigned int y, couleur_t c){
  point_t * p = malloc(sizeof(point_t));
  p->detruire = detruire_point;
  p->afficher = afficher_point;
  p->x = x;
  p->y = y;
  p->couleur = c;
  return p;
}
