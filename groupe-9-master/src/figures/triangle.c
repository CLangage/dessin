#include"triangle.h"
#include"segment.h"


void detruire_triangle(void * triangle){
  triangle_t * t = triangle;
  free(t);
}


void afficher_triangle(SDL_Renderer *rendu, void * triangle){
    triangle_t * t = triangle;
 	  segment_t * s1 = creer_segment(t->p1, t->p2, t->couleur);
	  segment_t * s2 = creer_segment(t->p2, t->p3, t->couleur);
	  segment_t * s3 = creer_segment(t->p1, t->p3, t->couleur);
	  s1->afficher(rendu,s1);
	  s2->afficher(rendu, s2);
	  s3->afficher(rendu,s3);
	  s1->detruire(s1);
	  s2->detruire(s2);
	  s3->detruire(s3);


}

triangle_t * creer_triangle(point_t *p1,point_t *p2,point_t *p3 ,couleur_t c){
  triangle_t * t = malloc(sizeof(triangle_t));
  t->detruire = detruire_triangle;
  t->afficher = afficher_triangle;
  t->p1 = p1;
  t->p2 = p2;
  t->p3 = p3;
  t->couleur = c;
  return t;
}
