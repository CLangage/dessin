#include<stdio.h>
#include <stdlib.h>
#include"polygone.h"
#include"point.h"
#include"segment.h"
#include "SDL2/SDL.h"

int my_rand (void)
{
   return (rand ());
}
float* equation_droite( point_t a,  point_t b)
{
	float* crd = malloc(sizeof(float)*3);
	if(a.x != b.x){
		float m =(b.y-a.y)/(b.x-a.x);
		crd[0] = 1;
		crd[1] = m;
		crd[2] = a.y - (a.x*m);
	}
	else{
		crd[0] = 0;
		crd[1] = -1;
		crd[2] = a.x;}
	return crd;
}
int appartenance_p(float* coefs,  point_t p)
{
	if(((p.y*coefs[0])-(p.x*coefs[1])-coefs[2])==0)
		return 1;
	return 0;
}


void afficher_polygone(SDL_Renderer  * rendu, void * polygone)
{ polygone_t * pol = polygone;

   point_t * pnt =creer_point(0,0,pol->couleur);
  *pnt=*pol->pointDepart;
  int i=0;
  float* coefs = malloc(sizeof(float)*3 );
  point_t * pTmp =creer_point(0,0,pol->couleur);
  pTmp->x = my_rand()%700;
  pTmp->y = my_rand()%500;
  segment_t * s= creer_segment(pnt,pTmp , pol->couleur);
  s->afficher(rendu,s);
   printf ("p1 : (%d,%d)",pTmp->x,pTmp->y);
   printf ("p0 : (%d,%d)",pnt->x,pnt->y);
  for( i=0;i<pol->nb_cotes-1;i++ )
     {
       coefs=equation_droite(*pnt,*pTmp);
       *pnt=*pTmp;
       pTmp->x=my_rand()%700;
       pTmp->y=my_rand()%500;

       int j=0;
       while(appartenance_p(coefs,*pTmp)==1){
                j++;
  	        pTmp->x=my_rand()%700;
            pTmp->y=my_rand()%500;
        }
     printf("j:%d",j);
     printf ("p%d : (%d,%d)",i+2,pTmp->x,pTmp->y);
        if(i==pol->nb_cotes-2)
	       pTmp= pol->pointDepart;
	    s->p1->x=pnt->x;  s->p1->y=pnt->y;
	    s->p2->x=pTmp->x;  s->p2->y=pTmp->y;
     printf ("p%d : (%d,%d)",i+2,pTmp->x,pTmp->y);
	s->afficher(rendu,s);
    }

    free(coefs);

}


void detruire_polygone(void * polygone){
  polygone_t * p = polygone;
  free(p);
}

polygone_t * creer_polygone(point_t * pointDepart ,int nb_cotes, couleur_t c)
{
  polygone_t * polygone = malloc(sizeof(polygone_t ));
  polygone->afficher = afficher_polygone;
  polygone->detruire= detruire_polygone;
  polygone->pointDepart = pointDepart;
  polygone->nb_cotes = nb_cotes;
  polygone->couleur = c;

  return polygone;
}
