#ifndef __RECTANGLE_H__
#define __RECTANGLE_H__
#include"../couleur.h"
#include"point.h"
#include <SDL2/SDL.h>

typedef struct rectangle_s{
  void (*detruire)(void *);
  void (*afficher)(SDL_Renderer *, void *);
  point_t * pointDepart;
  unsigned int w;
  unsigned int h;
  couleur_t couleur;
}rectangle_t;

/**
   Cette fonction initialise une figure de type rectangle_t.
   Un rectangle possède 2 segments, une couleur et
   deux pointeurs de fonctions qui permettent de l'afficher et de le détruire.
   @param pointDepart est le point original.
   @param w est la largeur.
   @param h est la longueur.
   @param couleur est la couleur du rectangle.
   @requires pointDepart a été crée par la fonction creer_point.
   @requires c a été crée par la fonction creer_couleur.
   @requires w variable de type int.
   @require h variable de type int.
   @ensures resultat->detruire rectangle vers une fonction de libération de l'espace mémoire.
   @ensures resultat->afficher rectangle vers une fonction d'affichage.
   @ensures resultat->pointDepart=p,resultat->w=w,resultat->h=h,resultat->couleur = c.
   @return rectangle_t
*/
rectangle_t * creer_rectangle(point_t * p,unsigned int w,unsigned int h,couleur_t c);










#endif
