#ifndef __SEGMENT_H__
#define __SEGMENT_H__
#include"../couleur.h"
#include"point.h"
#include <SDL2/SDL.h>


typedef struct segment_s{
  void (*detruire)(void *);
  void (*afficher)(SDL_Renderer *, void *);
  point_t * p1;
  point_t * p2;
  couleur_t couleur;
}segment_t;


/**
   fonction initialise une figure de type segment_t.
   Un segment possede deux points, une couleur et
   deux pointeurs de fonctions qui permettent de l'afficher et de le detruire.
   @param p1 est le de depart de segment.
   @param p2 est le d'arrive de segment.
   @param couleur est la couleur du segment.
   @requires p1 a ete cree par la fonction creer_point.
   @requires p2 a ete cree par la fonction creer_point.
   @requires c a ete cree par la fonction creer_couleur.
   @ensures resultat->detruire pointe vers une fonction de libération de l'espace mémoire.
   @ensures resultat->afficher pointe vers une fonction d'affichage.
   @ensures resultat->x = x, resultat->y = y, resultat->couleur = c
   @return segment_t
*/
segment_t * creer_segment(point_t * p1 ,point_t * p2, couleur_t c);





#endif
