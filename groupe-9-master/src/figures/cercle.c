#include<stdio.h>
#include"cercle.h"
#include"point.h"


void tracer_cercle(SDL_Renderer  ** renderer,void * cercle)
{
  cercle_t * c = cercle;
  int cx = c->pointCentre->x;
  int cy = c->pointCentre->y;
  point_t * p = c->pointCentre;
  int rayon = c->r;
  int d, y, x;

  d = 3 - (2 * rayon);
  x = 0;
  y = rayon;

  while (y >= x) {
  	p->x =cx+x; p->y =cy+y; p->afficher(*renderer,p);
  	p->x =cx+y; p->y =cy+x; p->afficher(*renderer,p);
  	p->x =cx-x; p->y =cy+y; p->afficher(*renderer,p);
   	p->x =cx-y; p->y =cy+x; p->afficher(*renderer,p);
    p->x =cx+x; p->y =cy-y; p->afficher(*renderer,p);
  	p->x =cx+y; p->y =cy-x; p->afficher(*renderer,p);
   	p->x =cx-x; p->y =cy-y; p->afficher(*renderer,p);
    p->x =cx-y; p->y =cy-x; p->afficher(*renderer,p);

    if (d < 0)
      d = d + (4 * x) + 6;
    else {
      d = d + 4 * (x - y) + 10;
      y--;
    }

    x++;
  }
  p->detruire(p);
   c->detruire(c);
}

void afficher_cercle(SDL_Renderer  * rendu, void * cercle)
{
   cercle_t * c = cercle;
   tracer_cercle(&rendu,c);
   c->detruire(c);
}

void detruire_cercle(void * cercle){
  cercle_t * c = cercle;
  free(c);
}

cercle_t * creer_cercle(point_t * pointCentre ,int r, couleur_t c)
{
  cercle_t * cercle = malloc(sizeof(cercle_t ));
  cercle->afficher = afficher_cercle;
  cercle->detruire= detruire_cercle;
  cercle->pointCentre = pointCentre;
  cercle->r = r;
  cercle->couleur = c;

  return cercle;
}
