#ifndef __TRIANGLE_H__
#define __TRIANGLE_H__
#include"../couleur.h"
#include"point.h"
#include <SDL2/SDL.h>


typedef struct triangle_s{
  void (*detruire)(void *);
  void (*afficher)(SDL_Renderer *, void *);
  point_t *p1;
  point_t *p2;
  point_t *p3;
  couleur_t couleur;
}triangle_t;

/**
   Cette fonction initialise une figure de type triangle_t.
   Un triangle possède 3 segments, une couleur et
   deux pointeurs de fonctions qui permettent de l'afficher et de le détruire.
   @param p1 est le premier point.
   @param p2 est le deuxième point.
   @param p3 est le troisième point.
   @param couleur est la couleur du point.
   @requires c a été crée par la fonction creer_couleur.
   @requires p1 a été crée par la fonction creer_point.
   @requires p2 a été crée par la fonction creer_point.
   @requires p3 a été crée par la fonction creer_point.
  
   @ensures resultat->detruire triangle vers une fonction de libération de l'espace mémoire.
   @ensures resultat->afficher triangle vers une fonction d'affichage.
   @ensures resultat->p1 = p1, resultat->p2 = p2,resultat->p3 = p3, resultat->couleur = c
   @return triangle_t;
*/
triangle_t * creer_triangle(point_t *p1,point_t *p2,point_t *p3, couleur_t c);





#endif
