#ifndef __POLYGONE_H__
#define __POLYGONE_H__
#include"../couleur.h"
#include"point.h"
#include <SDL2/SDL.h>


typedef struct polygone_s{
  void (*detruire)(void *);
  void (*afficher)(SDL_Renderer *, void *);
  point_t * pointDepart;
  int nb_cotes;
  couleur_t couleur;
}polygone_t;


/**
   fonction initialise une figure de type polygone_t.
   Un rectangle posséde un point de départ,les nombres de cotes, une couleur et
   deux pointeurs de fonctions qui permettent de l'afficher et de le detruire.
   @param pointDepart est le point de depart de polygone.
   @param nb_cotes est le nombres de cotes qu'il posséde.
   @param couleur est la couleur du polygone.
   @requires pointDepart a ete cree par la fonction creer_point.
   @requires nb_cotes de type int.
   @requires c a ete cree par la fonction creer_couleur.
   @ensures resultat->detruire polygone vers une fonction de libération de l'espace mémoire.
   @ensures resultat->afficher polygone vers une fonction d'affichage.
   @ensures resultat->pointDepart = pointDepart, resultat->nb_cotes =nb_cotes, resultat->couleur = c
   @return polygone_t
*/
polygone_t * creer_polygone(point_t * pointDepart ,int nb_cotes, couleur_t c);





#endif
