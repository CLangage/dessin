#include<stdio.h>
#include"segment.h"
#include"point.h"



void detruire_segment(void * segment){
  segment_t * s = segment;
  free(s);
}

void tracerSegment(SDL_Renderer * rendu ,unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, couleur_t c) {
	int  x, y, dx, dy ; 
	
    double e; ;  // valeur d�erreur et incr�ments
     dy= y2 - y1 ;
     dx= x2 - x1 ;
     y = y1 ;  // rang�e initiale
     e =-dx;  // valeur d�erreur initiale
     SDL_SetRenderDrawColor(rendu, c.rouge, c.vert, c.bleu, 255);
     for( x=x1;x<=x2;x++)
      {   
          SDL_RenderDrawPoint(rendu, x, y);
		      e=e + (2*dy);
	        if (e >= 0)  // erreur pour le pixel suivant de m�me rang�e
		      { y =  y + 1 ;  // choisir plut�t le pixel suivant dans la rang�e sup�rieure
		      e =  e - (2*dx) ;  // ajuste l�erreur commise dans cette nouvelle rang�e
		      }
      } 
	
}

void afficher_segment(SDL_Renderer * rendu, void * segment){
  segment_t * s = segment;
   tracerSegment(rendu, s->p1->x, s->p1->y, s->p2->x, s->p2->y,s->couleur);
   
}


segment_t * creer_segment(point_t * p1 ,point_t * p2, couleur_t c)
{
  segment_t * segment = malloc(sizeof(segment_t ));
  segment->afficher = afficher_segment;
  segment->detruire = detruire_segment;
  segment->p1 = p1;
  segment->p2 = p2;
  segment->couleur = c;
  return segment;
}
