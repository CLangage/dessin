#include"rectangle.h"
#include"point.h"
#include"segment.h"



void detruire_rectangle(void * rectangle){
  rectangle_t * r = rectangle;
  free(r);
}

void afficher_rectangle(SDL_Renderer  * rendu, void * rectangle){
  rectangle_t * r = rectangle;
  point_t * p = r->pointDepart;
  point_t * p1 = creer_point((p->x+r->w), p->y, r->couleur);
	point_t * p2 = creer_point((p1->x), (p1->y+r->h), r->couleur);
	point_t * p3 = creer_point(p->x, (p->y+r->h), r->couleur);
	segment_t * s1 = creer_segment(p, p1, r->couleur);
	segment_t * s2 = creer_segment(p1, p2, r->couleur);
	segment_t * s3 = creer_segment(p2, p3, r->couleur);
	segment_t * s4 = creer_segment(p3, p, r->couleur);

	s1->afficher(rendu,s1);
	s2->afficher(rendu, s2);
	s3->afficher(rendu,s3);
	s4->afficher(rendu,s4);

	s1->detruire(s1);
	s2->detruire(s2);
	s3->detruire(s3);
	s4->detruire(s4);
	p1->detruire(p1);
	p2->detruire(p2);
	p3->detruire(p3);

	p->detruire(p);
	r->detruire(r);

}
rectangle_t * creer_rectangle(point_t *p,unsigned int w,unsigned int h,couleur_t c)
{
  rectangle_t * r = malloc(sizeof(rectangle_t));
  r->pointDepart = p;
  r->detruire = detruire_rectangle;
  r->afficher = afficher_rectangle;
  r->w = w;
  r->h =h;
  r->couleur = c;
  return r;
}
