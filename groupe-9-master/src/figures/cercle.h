#ifndef __CERCLE_H__
#define __CERCLE_H__
#include"../couleur.h"
#include"point.h"
#include <SDL2/SDL.h>


typedef struct cercle_s{
  void (*detruire)(void *);
  void (*afficher)(SDL_Renderer *, void *);
  point_t * pointCentre;
  int r;
  couleur_t couleur;
}cercle_t;


/**
   fonction initialise une figure de type cercle_t.
   Un cercle possede un point de centre,rayon, une couleur et
   deux pointeurs de fonctions qui permettent de l'afficher et de le detruire.
   @param pointCentre est le point de centre d'un cercle.
   @param r est le rayon de cercle.
   @param couleur est la couleur du cercle.
   @requires pointCentre a ete cree par la fonction creer_point.
   @requires r rayon .
   @requires c a ete cree par la fonction creer_couleur.
   @ensures resultat->detruire cercle vers une fonction de libération de l'espace mémoire.
   @ensures resultat->afficher cercle vers une fonction d'affichage.
   @ensures resultat->pointCentre = pointCentre, resultat->r = r, resultat->couleur = c
   @return cercle_t
*/
cercle_t * creer_cercle(point_t * pointCentre ,int r, couleur_t c);





#endif
