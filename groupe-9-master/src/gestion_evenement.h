/**
Projet L2 Structures Algébriques 2022
Auteur : Mohamed Akaddar
*/

#ifndef __GESTION_EVENEMENTS_H__
#define __GESTION_EVENEMENTS_H__

#include"couleur.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include"liste_figure.h"

void gestion_evenements(int * close, SDL_Renderer* rendu, liste_figure_t* l,SDL_Surface* surface, SDL_Texture* texture, SDL_Rect rect, int* indice);




#endif
