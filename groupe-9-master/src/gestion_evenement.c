/**
Projet L2 Structures Algébriques 2022
Auteur : Mohamed Akaddar
*/

#include"gestion_evenement.h"




void gestion_evenements(int * close, SDL_Renderer* rendu, liste_figure_t* l,SDL_Surface* surface, SDL_Texture* texture, SDL_Rect rect, int* indice){
  SDL_Event event;


  // Events management
  while (SDL_PollEvent(&event)) {


    switch (event.type) {
    case SDL_KEYUP:
      switch (event.key.keysym.sym){

        case SDLK_q:
        	printf("Au revoir\n");
        	*close = 1;
  	      break;

        case SDLK_a:
        /* raccourci ”Couleur” */
          *indice = 1;
          printf("Préciser une couleur\n");
          unsigned int R, V, B;
          do{
            printf("\tMerci de preciser (RVB) la couleur avec des valeurs entre 0 et 255 :\n");
            printf("\t\t saisir valeur R = ");
            scanf("%u",&R);
            printf("\t\t saisir valeur V = ");
            scanf("%u",&V);
            printf("\t\t saisir valeur B = ");
            scanf("%u",&B);
          }while(R>255 || R<0 || V>255 || V<0 || B>255 || B<0);
          printf("R=%u\nV=%u\nB=%u",R,V,B);
          SDL_SetRenderDrawColor(rendu,R,V,B,255);
          printf("Vous pouvez désormais déssiner avec cette couleur\n");
          break;

        case SDLK_p:
         /*raccourcis permettant de dessiner des points*/
          *indice = 2;
          printf("Points\n");
          break;

        case SDLK_c:
        /*raccourcis permettant de dessiner des cercles*/
          *indice = 3;
          printf("Cercles\n");
     	    break;

        case SDLK_r:
         /*raccourcis permettant de dessiner des rectangles*/
          *indice = 4;
          printf("Rectangles\n");
         	break;

        case SDLK_t:
        /*raccourcis permettant de dessiner des triangles*/
        *indice = 5;
          printf("Triangles\n");
          break;

        case SDLK_l:
          /*raccourcis permettant de dessiner des segments*/
          *indice = 6;
          printf("segments\n");
        break;

        case SDLK_s:
        *indice = 7;
          printf("Sauvegarder le dessin\n");
          FILE* file = NULL;
          printf("Donner le chemin du fichier");
          //exemple : "img/me.bmp"
          char chemin[50];
          scanf("%s",chemin);
          if((file = fopen(chemin,"w")))
          {
            fclose(file);
            break;
          }
          SDL_SaveBMP(surface,chemin);
          break;

        case SDLK_d:
        *indice = 8;
        /* raccourcis permettant de charger un dessin / une image*/
          printf("Charger une image\n");
          printf("Donner le chemin du fichier");
          //exemple : "img/me.bmp"
          char path[50];
          scanf("%s",path);

          surface = SDL_LoadBMP(path);
          if(surface==NULL)
          {
            SDL_Log("Erreur > %s\n", SDL_GetError());
            exit(EXIT_FAILURE);
          }
          texture = SDL_CreateTextureFromSurface(rendu, surface);
          /*suppression de toutes la liste des figures actuellement affich´ees*/
          SDL_FreeSurface(surface);
          SDL_RenderClear(rendu);

          /*remplace par la ou les nouvelles figures*/
          SDL_RenderCopy(rendu, texture, NULL, &rect);
          SDL_RenderPresent(rendu);
          if(texture == NULL)
          {
            SDL_Log("Erreur > %s\n", SDL_GetError());
            exit(EXIT_FAILURE);
          }
          break;
        case SDLK_x:
          *indice = 9;
          printf("Dessin des polygones\n");
          break;
      }
      break;

    case SDL_MOUSEBUTTONDOWN:
      printf("Vous avez cliqué sur la souris :\n");
      printf("\tfenêtre : %d\n",event.button.windowID);
      printf("\tbouton : %d\n",event.button.button);
      printf("\tposition : %d;%d\n",event.button.x,event.button.y);



      /*Dessiner des points*/
      if(*indice == 2)
      {
        point_t* p = creer_point(event.button.x,event.button.y,creer_couleur(255,255,255));
        liste_inserer_fin(l,p);
      }

      /*Dessiner des cercles*/
      if(*indice == 3)
      {
        point_t* centre = creer_point(event.button.x,event.button.y,creer_couleur(255,255,255));
        cercle_t* cercle = creer_cercle(centre,30,creer_couleur(255,255,255));
        liste_inserer_fin(l,cercle);
      }

      /*Dessiner des rectangles*/
      if(*indice == 4)
      {
        rectangle_t *t=creer_rectangle(creer_point(event.button.x,event.button.y,creer_couleur(0,0,255)),100,200,creer_couleur(0,0,255));
        liste_inserer_fin(l,t);
      }

      /*Dessiner des triangles*/
      if(*indice == 5)
      {
        point_t* p = creer_point(event.button.x,event.button.y,creer_couleur(255,255,255));
        point_t* p1 = creer_point(event.button.x+100,event.button.y,creer_couleur(255,255,255));
        point_t* p2 = creer_point(event.button.x+50,event.button.y+200,creer_couleur(255,255,255));
        liste_inserer_fin(l,creer_triangle(p,p1,p2,creer_couleur(0,255,255)));
      }

      /*Dessiner des segments*/
      if(*indice == 6)
      {
        point_t* p = creer_point(event.button.x,event.button.y,creer_couleur(255,255,255));
        point_t* p1 = creer_point(event.button.x+200,event.button.y+100,creer_couleur(255,255,255));
        liste_inserer_fin(l,creer_segment(p,p1,creer_couleur(255,255,255)));
      }
      break;



      /*dessin des polygones*/
      if(*indice == 9)
      {
        point_t* point_dep = creer_point(event.button.x,event.button.y,creer_couleur(255,255,255));
        liste_inserer_fin(l, creer_polygone(point_dep, 5, creer_couleur(255,255,255)));
      }

    case SDL_QUIT:
      printf("Au revoir\n");
      *close = 1;
      break;
    }

  }
}
